# mastodon-toot-deleter

Using package ```Mastodon.py```

## Usage

1. Go to **Settings-Development-Your applications**, then create an app tiking boxes of **read** and **write**.
2. You should get **Client key**, **Client secret** and **Your access token** in the application page. Fill them to the corresponding fields in ```config.ini```.
    >   note that the fields are named as ```app_id```, ```secret``` and ```token``` in ```config.ini```.
3. **Double check** if you want to delete all your toots, and you are good to go.

Any regular expression rules can be added to ```toot.content``` in order to delete the toot you want. This python script is only a simple demo.

## Note
Mastodon has api rate limits, the deletion process might take a looooooong time. (It might also crash during the process...) 
