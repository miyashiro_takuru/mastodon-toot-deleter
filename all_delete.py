# pip install -r requirements.txt

# created by miyashiro_takuru on 03/20/2019
# modified from https://github.com/yakumo-saki/mastodon_toot_delete

# noinspection PyPackageRequirements
from mastodon import Mastodon
from configparser import ConfigParser

config = ConfigParser()
config.read('config.ini')

access_token = config['instance']['token']
client_id = config['instance']['app_id']
client_secret = config['instance']['secret']
api_base_url = config['instance']['url']

instance = Mastodon(
    access_token = access_token,
    client_id = client_id,
    client_secret = client_secret,
    api_base_url = api_base_url,
    ratelimit_method = 'pace'
)

account = instance.account_verify_credentials()

toots = instance.account_statuses(account)

while toots is not None:
    for toot in toots:
        print(str(toot.id) + ": " + toot.content)
        instance.status_delete(toot)

    print("Single page done")
    toots = instance.fetch_next(toots)

print("All done")
